
def import_class(name):
    module, classname = name.rsplit(".", 1)
    mod = __import__(module, fromlist=[classname])
    return getattr(mod, classname)
