import sys; sys.path.insert(0, '.')  # @IgnorePep8
if 'threading' in sys.modules:
    raise Exception('threading module loaded before patching!')

from gevent import monkey; monkey.patch_all()  # @IgnorePep8

from django.conf import settings
from django.contrib.auth.middleware import AuthenticationMiddleware
from django.contrib.sessions.middleware import SessionMiddleware
from django.core.handlers.wsgi import WSGIRequest, WSGIHandler
from django.utils import autoreload
from socketio import socketio_manage
from socketio.handler import SocketIOHandler
from socketio.server import SocketIOServer
from socketio_server.options import DispatchingNamespace
from threading import Lock
from utils import import_class
import _socket
import os


lock = Lock()


def unlink(path):
    from errno import ENOENT
    try:
        os.unlink(path)
    except OSError, ex:
        if ex.errno != ENOENT:
            raise


def link(src, dest):
    from errno import ENOENT
    try:
        os.link(src, dest)
    except OSError, ex:
        if ex.errno != ENOENT:
            raise


def bind_unix_listener(path, backlog=50):
    pid = os.getpid()
    tempname = os.path.abspath('%s.%s.tmp' % (path, pid))
    backname = os.path.abspath('%s.%s.bak' % (path, pid))
    unlink(tempname)
    unlink(backname)
    link(path, backname)
    try:
        sock = _socket.socket(_socket.AF_UNIX, _socket.SOCK_STREAM)
        sock.setblocking(0)
        sock.bind(tempname)

        os.chmod(tempname, 0777)
        sock.listen(backlog)
        os.rename(tempname, path)
        return sock
    finally:
        unlink(backname)


class ProxiedWSGIHandler(SocketIOHandler):

    def get_environ(self):
        if "X-Real-IP" in self.headers:
            self.client_address = (self.headers.get('X-Real-IP', '1.1.1.1'), '')
        return super(ProxiedWSGIHandler, self).get_environ()


def not_found(start_response):
    print "not found"
    start_response('404 Not Found', [])
    return ['<h1>Not Found</h1>']


def load_namespaces():
    namespace_module = getattr(settings, "NAMESPACE_MODULE", None)
    if not namespace_module:
        namespace_module = settings.ROOT_URLCONF.rsplit(".")[0] + ".namespaces"

    if namespace_module:
        try:
            namespaces = import_class(namespace_module).namespaces
            return namespaces
        except Exception, ex:
            pass

    return {}


class Application(object):
    _my_namespaces = {
        '/default': DispatchingNamespace
    }

    def __init__(self):
        namespaces = load_namespaces()
        if namespaces:
            self._my_namespaces = namespaces

        self.django_wsgi = WSGIHandler()
        super(Application, self).__init__()
        self.middleware = [
            SessionMiddleware(),
            AuthenticationMiddleware()
        ]

    def __call__(self, environ, start_response):
        path = environ['PATH_INFO']

        if path.startswith('/socket.io/static/'):
            try:
                filename = os.path.join(settings.STATIC_ROOT,
                    path.replace("/socket.io/static/", ""))
                data = open(filename).read()
            except Exception, ex:
                print ex
                return not_found(start_response)

            if path.endswith(".js"):
                content_type = "text/javascript"
            elif path.endswith(".css"):
                content_type = "text/css"
            elif path.endswith(".swf"):
                content_type = "application/x-shockwave-flash"
            else:
                content_type = "text/html"

            start_response('200 OK', [('Content-Type', content_type)])
            return [data]

        if "socketio" not in environ:
            return self.django_wsgi(environ, start_response)
        else:
            request = WSGIRequest(environ)
            for middle in self.middleware:
                middle.process_request(request)
            socketio_manage(environ, self._my_namespaces, request=request)


def serve(interface=('0.0.0.0', 8000), auto_reload=False, **kwargs):
    if auto_reload:
        autoreload.main(_serve, [interface], kwargs)
    else:
        _serve(interface, **kwargs)


def _serve(interface=('0.0.0.0', 8000), **kwargs):
    if not isinstance(interface, (list, tuple)):
        interface = bind_unix_listener(interface)

    print
    print "Listening on %s" % (interface,)
    print "=" * 20
    print

    SocketIOServer(interface, Application(),
        policy_server=False,
        **kwargs).serve_forever()
